import React from 'react';
import Search from './containers/Search';
import Topic from './components/Topic';

import './style.css';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.myRef = React.createRef();
  }

  onClick = e => {
    this.myRef.current.scrollIntoView();
  };

  render() {
    let arr = [];
    for (let i = 0; i < 300; i++) {
      arr.push(i);
    }

    return (
      <React.Fragment>
        <div className="header">
          <Search />
        </div>
        <div>
          {arr
            .filter(el => {
              return el % 2 === 0 ? el : false;
            })
            .filter(el => {
              return el % 4 === 0 ? el : false;
            })
            .map((el, index) => {
              if (index === 40) {
                return (
                  <div
                    key={index}
                    ref={this.myRef}
                    onClick={e => this.onClick(e)}
                  >
                    {el}
                  </div>
                );
              }
              return <div key={index}>{el}</div>;
            })}
        </div>
      </React.Fragment>
    );
  }
}
