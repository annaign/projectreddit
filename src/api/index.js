import axios from 'axios';

const baseUrl = 'https://www.reddit.com/';

//упрощенная запись
export const searchReddit = ({ search, limit = 25 }) =>
  axios.get(`${baseUrl}search.json?q=${search}&limit=${limit}`);

export const popularReddid = ({ limit = 100 }) => {
  return axios.get(`${baseUrl}subreddits/popular.json?limit=${limit}`);
};
