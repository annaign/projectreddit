import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import * as actions from '../store/actions/searchText';
import * as actions2 from '../store/actions/showTopic';
import Input from '../components/Input';
import Button from '../components/Button';
import { searchReddit } from '../api';

import './style.css';

// Какие свойства будут связаны между store и компонентом (props)
const mapStateToProps = state => {
  return { search: state.search.get('search') };
};

// Какие actions будут доступны компоненту
const mapDispatchToProps = dispatch => ({
  getSearchRequest: search => {
    dispatch(actions.getSearchRequest(search));
  },
  getSearchSuccess: arr => {
    dispatch(actions.getSearchSuccess(arr));
  },
  showTopic: ({ linkToArr, index }) => {
    dispatch(actions2.showTopic({ linkToArr, index }));
  }
});

class Search extends React.Component {
  static propTypes = {
    getSearchRequest: PropTypes.func.isRequired,
    getSearchSuccess: PropTypes.func.isRequired,
    showTopic: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);
    this.state = {
      search: ''
    };
  }

  onSearchClickHandler = () => {
    const { search } = this.state;
    const { getSearchRequest, getSearchSuccess, showTopic } = this.props;

    getSearchRequest(search);
    searchReddit({ search }).then(response =>
      getSearchSuccess(response.data.data.children)
    );

    showTopic({ index: 0, linkToArr: 'search' });
  };

  onSearchChangeHandler = search => {
    this.setState({ search });
  };

  render() {
    const { search } = this.state;
    return (
      <div className="searchBlock">
        <Input
          value={search}
          onChange={this.onSearchChangeHandler}
          placeholder="Введите текст..."
        />
        <Button onClick={this.onSearchClickHandler}>Поиск</Button>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Search);

// import { SearchRequest } from 'actions';
// const DataSelector = state => state.search.arr;
// export const getData = createStructuredSelector({
//  data: DataSelector,
//})
// @connect(getData)
// const { data, dispatch } = this.props;
// dispatch(SearchRequest());
