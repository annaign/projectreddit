import { fromJS } from 'immutable';
import * as constants from '../../constants';

const initialState = fromJS({
  linkToArr: '', //state.search
  index: -1 //индекс topic в массиве
});

function paginationReducer(state = initialState, action) {
  switch (action.type) {
    case constants.SHOW_TOPIC:
      return state
        .set('linkToArr', action.payload.linkToArr)
        .set('index', action.payload.index);
    default:
      return state;
  }
}

export default paginationReducer;
