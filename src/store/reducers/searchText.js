import { fromJS } from 'immutable';
import * as constants from '../../constants';

const initialState = fromJS({
  arr: [],
  search: '',
  isLoading: false
});

function searchTextReducer(state = initialState, action) {
  switch (action.type) {
    case constants.SEARCH_TEXT_REQUEST:
      return state.set('isLoading', true).set('search', action.payload.search);
    case constants.SEARCH_TEXT_SUCCESS:
      return state
        .set('isLoading', false)
        .update('arr', arr => arr.merge(fromJS(action.payload.arr)));
    default:
      return state;
  }
}

export default searchTextReducer;
