import { combineReducers } from 'redux';
import searchTextReducer from './searchText';
import paginationReducer from './pagination';

const reducers = combineReducers({
  search: searchTextReducer,
  pagination: paginationReducer
});

export default reducers;
