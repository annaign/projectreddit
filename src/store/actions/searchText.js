import * as actions from '../../constants';

export const getSearchRequest = search => {
  return { type: actions.SEARCH_TEXT_REQUEST, payload: { search } };
};

export const getSearchSuccess = arr => {
  return { type: actions.SEARCH_TEXT_SUCCESS, payload: { arr } };
};
