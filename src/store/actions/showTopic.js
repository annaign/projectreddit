import * as actions from '../../constants';

const showTopic = ({ linkToArr, index }) => {
  return { type: actions.SHOW_TOPIC, payload: { linkToArr, index } };
};

export default showTopic;
