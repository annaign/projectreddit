import React from 'react';
import PropTypes from 'prop-types';

import './style.css';

export default function Input(props) {
  const { type, size, placeholder, value, onChange } = props;

  const onChangeHandler = event => {
    onChange(event.target.value);
  };

  return (
    <div>
      <input
        className="inputSearch"
        type={type}
        size={size}
        value={value}
        onChange={onChangeHandler}
        placeholder={placeholder}
      />
    </div>
  );
}

Input.propTypes = {
  type: PropTypes.string,
  size: PropTypes.number,
  placeholder: PropTypes.string,
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired
};

Input.defaultProps = {
  type: 'text',
  size: 40,
  placeholder: ''
};
