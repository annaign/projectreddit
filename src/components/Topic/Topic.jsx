import React from 'react';
import PropTypes from 'prop-types';

import './style.css';

export default function Topic(props) {
  const { urlImg, title } = props;

  return (
    <div className="topicBlock">
      <img src={urlImg} alt="" className="topicImg" />
      <h2 className="titleTopic">{title}</h2>
    </div>
  );
}

Topic.propTypes = {
  urlImg: PropTypes.string,
  title: PropTypes.string
};

Topic.defaultProps = {
  urlImg: '#',
  title: ''
};
